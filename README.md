# PHP CMS

A PHP application for CRUD operations for posts, post types, and categories. This is a basic CMS, with some inspiration taken from WordPress.

### Features

- MVC pattern
- Pages
- Post types and archives
- Authenticated login sessions for users
- Admin dashboard
- TinyMCE editor
- Templates

### Technologies

- Apache
- MySQL
- phpMyAdmin
- Docker

## Installation

For this to work, you just need to:

- create an env file
- run the containers
- upload the database backup file to phpMyAdmin

Check the `docker-compose.yml` file for which ports to use for each service.

### The .env file

Make an .env file in the root directory. This is shared by the containers.

    SITENAME=Site name
    URLROOT=http://localhost:the-port
    MYSQL_ROOT_PASSWORD=rootpassword
    MYSQL_DATABASE=php_app_db
    MYSQL_USER=php_app_dbusr
    MYSQL_PASSWORD=php_app_dbpass
    TMCE_KEY=apikey_0000000000

- **SITENAME**: This is the string which will appear in several different places in the app.
- **URLROOT**: The base URL for the project which is used in links throughout the app. Normally, the folder name of the project in the public access directory (htdocs, www, public_html) is used as the final segment in the URL.
- **TMCE_KEY**: You'll want to create an account at the [tinyMCE website](https://www.tiny.cloud) and generate an API key. Then come back here and print the key for this constant.

Run `docker-compose up -d --build` in the root directory.

### **Database**

Go to **phpMyAdmin** and use the credentials you entered in the `.env` file.

- Server: leave empty
- User: **MYSQL_USER**
- pass: **MYSQL_PASSWORD**

There will be a database made with the **MYSQL_DATABASE** variable as the name. on your local dev environment. There is a file called `sqldb.sql` in the root directory. import this file to the database and the app be able to work.

**A note on terms:** It's important to have a term with the id of **0**, as new posts have a term set to 0. Uncategorised would be appropriate here. If this is not present, new posts will not appear in the archives.

## How it works

This is a PHP app that uses the PDO class to perform CRUD operations on data from an SQL database. The MVC pattern is used to model data for posts, users, categories etc.

The `app/lib/` directory contains the following:

- The `Controller` base class provides methods for loading **models** and **views**.
- The `Admin_Controller` base class adds user login verification.
- The `Database` class provides CRUD functions for the database.
- The `Model` base class instantiates a `Database` object as a property.
- The `Init` class provides the initial response to the end user.

### MVC pattern

The .htaccess files block access to the `src/` folder, and sends the user to the `public/index.php` file. This is where the `bootstrap.php` file is required, which loads dependencies for the app; classes, configurations, etc.

The `init` class is then instantiated, which breaks the current URL into an array segments and query variables. These array items are used to instantiate a **controller**, and call methods when specified.

### Pages

There is no pages model. The pages controller simply loads the view with data declared in the class itself.

### Post types and posts

In the database, the `post_types` table has a column for the post type `id`. The `posts` table has a `post_type` column, with each row having an `id` that should correspond to that of a post type.

The application uses these `id` values to filter posts by post type, and to bind these rows from different tables together when necessary.

### Templates

In the base `Controller` class, there is the `view` method, which requires files based on arguments. There is one argument for the main view file, and another argument for the contextual view files; 'public', 'admin', etc. You can add options to this method as desired.

## **Credits**

- [TraversyMedia](https://www.traversymedia.com) - [**Object Oriented PHP & MVC**](https://www.udemy.com/course/object-oriented-php-mvc/learn/lecture/8283382?start=0#overview).
- https://medium.com/cranecloud/dockerizing-a-php-application-a35f56e7c0c2
- https://webmasters.stackexchange.com/questions/87926/mod-rewrite-ignore-stylesheet-and-javascript
