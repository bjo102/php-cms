-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 24, 2022 at 02:03 PM
-- Server version: 8.0.23
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bjo102`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int NOT NULL,
  `author` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `published` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_general_ci,
  `post_type` int NOT NULL DEFAULT '1',
  `term` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author`, `title`, `name`, `content`, `published`, `updated`, `excerpt`, `post_type`, `term`) VALUES
(120, 1, 'Health Plan Article', 'health-plan', '<h1>Status</h1>\r\n<p>height: 180cm</p>\r\n<h2>Current</h2>\r\n<p>weight: 70kg</p>\r\n<h1>Goals</h1>\r\n<p>weight: 80kg</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h1>Workout routine</h1>\r\n<p>Pushups</p>\r\n<p>Dumbells benchpress</p>\r\n<p>Barbell benchpress</p>\r\n<p>Overhead verticlal press</p>\r\n<p>barbell overhead press</p>\r\n<p>chess supported row</p>\r\n<p>tripod row</p>\r\n<p>underhand barbel row</p>\r\n<p>vertical pull</p>\r\n<p>pullups</p>\r\n<p>hindge pull</p>\r\n<p>romanian deadlifts</p>\r\n<p>deadlifts ***</p>\r\n<p>squats ***</p>\r\n<p>dumbell dropsquat</p>\r\n<p>goblet squat</p>\r\n<p>back squats</p>\r\n<p>static ludge</p>\r\n<p>split squat</p>\r\n<p>- body weight</p>\r\n<p>- weighted</p>\r\n<p>bulgarian split squat</p>\r\n<p>reverse lundge</p>\r\n<p>suitcase revers lundge</p>\r\n<p>rollups ***</p>\r\n<p>jackknife</p>\r\n<p>hanging knee raise</p>\r\n<p>learn how to carry weights/dumbells: one hand, the other, two hands, overhead</p>\r\n<h1>Diet</h1>\r\n<h2>Calories</h2>\r\n<p><span class=\"js-about-item-abstr\">The calorie is a unit of energy defined as the amount of heat needed to raise the temperature of a quantity of water by one degree.</span></p>\r\n<p>define base calorie intake</p>\r\n<p>weigh yourself 3 times a week</p>\r\n<p>&nbsp;</p>\r\n<h2>Phases</h2>\r\n<p><strong>Bulking:</strong> building muscle</p>\r\n<p>- Increase base calory intake by 15%</p>\r\n<p><strong>Cutting:</strong> moderating fat</p>\r\n<p>- Increase base calory intake by 15%</p>\r\n<p>Each meal and snack should contain 20&ndash;30 grams of protein to optimally support muscle building (<a class=\"content-link css-5r4717\" href=\"https://www.ncbi.nlm.nih.gov/pubmed/23459753/\" target=\"_blank\" rel=\"noopener noreferrer\">15<span class=\"css-1mdvjzu icon-hl-trusted-source-after\"><span class=\"sro\">Trusted Source</span></span></a>).</p>\r\n<p>When you&rsquo;re in a bulking phase, your food intake will be much higher than when you&rsquo;re in a cutting phase.</p>\r\n<p>You can enjoy the same foods in the cutting phase that you would when bulking &mdash; just in smaller portions.</p>\r\n<p>Don\'t gain or loose 0.5% of body weight per week, so as to avoid loosing muscle.</p>\r\n<p>A multi-vitamin and mineral supplement may be helpful if you&rsquo;re limiting your calorie intake in an effort to reduce body fat during your cutting phase.</p>\r\n<p>&nbsp;</p>\r\n<h2>Macronutrient Ratio</h2>\r\n<p>This is the ratio of carbohydrates, fats and protien in your diet.</p>\r\n<ul>\r\n<li>30&ndash;35% of your calories from protein</li>\r\n<li>55&ndash;60% of your calories from carbs</li>\r\n<li>15&ndash;20% of your calories from fat</li>\r\n</ul>\r\n<p>This doesn\'t change between diet <strong>phases</strong>.</p>\r\n<p>&nbsp;</p>\r\n<h2>Foods</h2>\r\n<ul class=\"hl-long-line\">\r\n<li><strong>Meats, poultry and fish: </strong>Sirloin steak, ground beef, pork tenderloin, venison, chicken breast, salmon, tilapia and cod.</li>\r\n<li><strong>Dairy: </strong>Yogurt, cottage cheese, low-fat milk and cheese.</li>\r\n<li><strong>Grains: </strong>Bread, cereal, crackers, oatmeal, <a class=\"content-link css-5r4717\" href=\"https://www.healthline.com/nutrition/11-proven-benefits-of-quinoa\">quinoa</a>, popcorn and rice.</li>\r\n<li><strong>Fruits:</strong> Oranges, apples, bananas, grapes, pears, peaches, watermelon and berries.</li>\r\n<li><strong>Starchy vegetables: </strong>Potatoes, corn, green peas, green lima beans and <a class=\"content-link css-5r4717\" href=\"https://www.healthline.com/nutrition/cassava\">cassava</a>.</li>\r\n<li><strong>Vegetables: </strong>Broccoli, spinach, leafy salad greens, tomatoes, green beans, cucumber, zucchini, asparagus, peppers and mushrooms.</li>\r\n<li><strong>Seeds and nuts:</strong> Almonds, walnuts, sunflower seeds, <a class=\"content-link css-5r4717\" href=\"https://www.healthline.com/nutrition/11-proven-health-benefits-of-chia-seeds\">chia seeds</a> and flax seeds.</li>\r\n<li><strong>Beans and legumes:</strong> Chickpeas, lentils, kidney beans, black beans and pinto beans.</li>\r\n<li><strong>Oils: </strong>Olive oil, flaxseed oil and avocado oil.</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p><strong>Supplements</strong></p>\r\n<ul class=\"hl-long-line\">\r\n<li><strong>Whey protein:</strong> Consuming whey protein powder is an easy and convenient way to increase your protein intake.</li>\r\n<li><strong>Creatine:</strong> <a class=\"content-link css-5r4717\" href=\"https://www.healthline.com/nutrition/10-benefits-of-creatine\">Creatine</a> provides your muscles with the energy needed to perform an additional rep or two. While there are many brands of creatine, look for creatine monohydrate as it&rsquo;s the most effective (<a class=\"content-link css-5r4717\" href=\"https://www.ncbi.nlm.nih.gov/pubmed/28615996\" target=\"_blank\" rel=\"noopener noreferrer\">12<span class=\"css-1mdvjzu icon-hl-trusted-source-after\"><span class=\"sro\">Trusted Source</span></span></a>).</li>\r\n<li><strong>Caffeine:</strong> <a class=\"content-link css-5r4717\" href=\"https://www.healthline.com/nutrition/caffeine-and-exercise\">Caffeine</a> decreases fatigue and allows you to work harder. It&rsquo;s found in pre-workout supplements, coffee or tea (<a class=\"content-link css-5r4717\" href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2824625/\" target=\"_blank\" rel=\"noopener noreferrer\">13<span class=\"css-1mdvjzu icon-hl-trusted-source-after\"><span class=\"sro\">Trusted Source</span></span></a>).</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h2>Suggested Meals</h2>\r\n<p><strong>Breakfasts</strong></p>\r\n<p>- Scrambled eggs with mushrooms and oatmeal</p>\r\n<p>- Protein pancakes with light-syrup, peanut butter and raspberries.</p>\r\n<p>- Chicken sausage with egg and roasted potatoes</p>\r\n<p>- Ground turkey, egg, cheese and salsa in a whole-grain tortilla</p>\r\n<p>- Blueberries, strawberries and vanilla Greek yogurt on overnight oats.</p>\r\n<p>&nbsp;</p>\r\n<p><strong>snacks</strong></p>\r\n<p>- Low-fat cottage cheese with blueberries</p>\r\n<p>- Protein shake and a banana.</p>\r\n<p>- Hard boiled eggs and an apple.</p>\r\n<p>- Protein shake and walnuts.</p>\r\n<p>- Greek yogurt and almonds.</p>\r\n<p>-&nbsp; Protein shake and grapes.</p>\r\n<p>- Yogurt with granola.</p>\r\n<p>- Protein shake and mixed berries</p>\r\n<p>- Jerky and mixed nuts.</p>\r\n<p>- Protein shake and watermelon.</p>\r\n<p>- Can of tuna with crackers.</p>\r\n<p>- Protein shake and pear.</p>\r\n<p>- Protein balls and almond butter.</p>\r\n<p>- Protein shake and strawberries.</p>\r\n<p><strong>Meals</strong></p>\r\n<p>-&nbsp; Turkey meatballs, marinara sauce and parmesan cheese over pasta.</p>\r\n<p>- Pork tenderloin slices with roasted garlic potatoes and green beans.</p>\r\n<p>- Diced beef with rice, black beans, bell peppers, cheese and pico de gallo.</p>\r\n<p>- Tilapia fillet, potato wedges and bell peppers.</p>\r\n<p>-&nbsp; Ground beef with corn, brown rice, green peas and green beans.</p>\r\n<p>- Tilapia fillets with lime juice, black and pinto beans and seasonal veggies.</p>\r\n<p>- <strong> </strong>Stir-fry with chicken, egg, brown rice, broccoli, peas and carrots.</p>\r\n<p>- Chicken breast, baked potato, sour cream and broccoli.</p>\r\n<p>- Mackerel, brown rice and salad leaves with vinaigrette.</p>\r\n<p>- Turkey breast, basmati rice and mushrooms.</p>\r\n<p>- Ground turkey and marinara sauce over pasta.</p>\r\n<p>- Sirloin steak, sweet potato and spinach salad with vinaigrette.</p>\r\n<p>- Salmon, quinoa and asparagus.</p>\r\n<p>- Venison burger, white rice and broccoli.</p>\r\n<p>&nbsp;</p>\r\n<h2><strong>Shopping list items</strong></h2>\r\n<p>Tinned Salmon<strong><br /></strong></p>\r\n<p>tinned makarel</p>\r\n<p>tinned tuna</p>\r\n<p>broccoli</p>\r\n<p>carrots</p>\r\n<p>quinola</p>\r\n<p>asparagus</p>\r\n<p>Turkey</p>\r\n<p>mushrooms</p>\r\n<p>eggs</p>\r\n<p>oatmeal</p>\r\n<p>protien shake powder</p>\r\n<p>banana</p>\r\n<p>greek yougart</p>\r\n<p>potatos</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', '2021-08-13 11:13:41', NULL, 'Getting healthy', 1, 0),
(137, 1, 'Post about your Life plan', 'life-plan', '<p>get old, die</p>', '2021-11-06 22:50:53', NULL, 'for the good life.', 5, 8),
(138, 1, 'Post of the posts post type', 'shamm', '<p>dsadsfd</p>', '2022-03-24 10:54:33', NULL, '', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `post_types`
--

CREATE TABLE `post_types` (
  `id` int NOT NULL,
  `single` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `plural` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `description` text COLLATE utf8mb4_general_ci NOT NULL,
  `taxonomy` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `post_types`
--

INSERT INTO `post_types` (`id`, `single`, `plural`, `name`, `description`, `taxonomy`) VALUES
(1, 'article', 'articles', 'articles', 'Articles are cool', '1'),
(5, 'post', 'posts', 'posts', 'Posts are also cool', '1');

-- --------------------------------------------------------

--
-- Table structure for table `taxonomies`
--

CREATE TABLE `taxonomies` (
  `id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `plural` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `description` text COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `taxonomies`
--

INSERT INTO `taxonomies` (`id`, `title`, `plural`, `name`, `description`) VALUES
(1, 'category', 'categories', 'category', 'The main taxonomy for this website.');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` int NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `taxonomy` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `title`, `name`, `taxonomy`) VALUES
(0, 'Uncategorized', 'uncategorized', 1),
(8, 'Toast', 'toast', 1),
(9, 'Jam toast', 'jam-toast', 1),
(10, 'Health', 'health', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created`) VALUES
(1, 'Byron', 'byronomalley950@gmail.com', '$2y$10$4dQRInAMf/kdTG/2P04jxeXNYccvqX.BTm1HpRncNevlwplsqbRAS', '2021-04-08 00:39:04'),
(3, 'joe', 'joe@hotmail.com', '$2y$10$561Wib6m3QjG1993qC9y0.8ZiG/rf9AA7ZACFpTOVTbBBYR3bCAOi', '2021-09-04 20:31:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_types`
--
ALTER TABLE `post_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxonomies`
--
ALTER TABLE `taxonomies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;

--
-- AUTO_INCREMENT for table `post_types`
--
ALTER TABLE `post_types`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `taxonomies`
--
ALTER TABLE `taxonomies`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
