<?php

/**
 * PHP CMS Public index file.
 * 
 * This is the file the user is driven through to the app.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Library
 */

require_once getenv('APPROOT') . 'src/bootstrap.php';

// Init Core Library
$init = new Init;

?>