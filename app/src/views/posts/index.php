<?php

/**
 * PHP CMS Views: Posts index page.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<section class="main-content-wrapper">
    <h1><?php echo $data['archive_title']; ?></h1>
    <p><?php echo $data['post_type_description']; ?></p>
    <?php get_posts_archive($data['posts']); ?>
</section>
