<?php

/**
 * PHP CMS Views: Posts archive.
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Views
 */

if ($posts) : ?>
    <div class="posts-archive">
        <?php foreach ($posts as $post) :
            $post_link = URLROOT . '/' . $post->postTypeName . '/' . $post->name;
            ?>
            <article class="post <?php echo slugify_string($post->postTypeSingle); ?>">
                <div class="post-details">
                    <a class="post-title" href="<?php echo $post_link; ?>">
                        <h4><?php echo $post->title; ?></h4>
                    </a>

                    <span class="post-date-published"><?php echo $post->published; ?></span>

                    <?php if ($post->excerpt) : ?>
                        <p class="post-excerpt"><?php echo $post->excerpt; ?></p>
                    <?php endif; ?>
                    <a class="post-read-more" href="<?php echo $post_link; ?>">Read More</a>
                </div>
            </article>
        <?php endforeach; ?>
    </div>
<?php endif;
