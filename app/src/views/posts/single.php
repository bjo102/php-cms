<?php

/**
 * PHP CMS Views: Posts single page tempalate.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<div class="main-content-wrapper">
    <h1><?php echo $data['post_title']; ?></h1>
    <section class="posts-content"><?php echo $data['content']; ?></section>
</div>