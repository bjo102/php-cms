<?php

/**
 * PHP CMS Views: Admin navbar.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<nav id="admin-main-nav" class="main-nav">
    <div class="main-nav-logo-container">
    </div>
    <div class="main-nav-links-container">
        <ul class="main-nav-links-list">
            <li>
                <a class="admin-nav-home-link" href="<?php echo URLROOT; ?>/admin"><?php echo SITENAME; ?>: Admin</a>
            </li>
            <li>
                <a class="admin-nav-home-link" href="<?php echo URLROOT; ?>">Landing Page</a>
            </li>
        </ul>
        <ul class="main-nav-links-list">
            <li>
                <a href="<?php echo URLROOT; ?>/logout">Logout</a>
            </li>
        </ul>
    </div>
</nav>