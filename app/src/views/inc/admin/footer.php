<?php

/**
 * PHP CMS Views: Admin footer.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

</main>
<footer class='footer'>
    <div class="footer-col">
        <p id="footer-quotes">We may just go where no one's been</p>
        <div>
</footer>
<script src="<?php echo URLROOT; ?>/js/admin.js"></script>
</body>

</html>