<?php

/**
 * PHP CMS Views: Public header.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="cononical" href="<?php echo get_canonical_url(); ?>">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/public/public.css">
    <title><?php echo SITENAME; ?></title>
</head>

<body>
    <?php require APPROOT . 'src/views/inc/public/navbar.php'; ?>
    <main class="main-content-container">