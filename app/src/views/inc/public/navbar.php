<?php

/**
 * PHP CMS Views: Public navbar.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

$pt = new Post_Type;
$pts = $pt->get_post_types();

?>

<nav class="main-nav">
    <div class="main-nav-logo-container">
        <a id="main-nav-logo" href="<?php echo URLROOT; ?>"><?php echo SITENAME; ?></a>
    </div>

    <div class="main-nav-links-container">
        <ul class="main-nav-links-list">
            <li>
                <a href="<?php echo URLROOT; ?>">Home</a>
            </li>
            <?php if (isset($_SESSION['user_name'])) : ?>
                <li>Welcome <?php echo $_SESSION['user_name'] ?></li>
                <li><a href="<?php echo URLROOT; ?>/admin">Admin</a></li>
            <?php endif; ?>
            <li>
                <a href="<?php echo URLROOT; ?>/about">About</a>
            </li>
        </ul>

        <ul class="main-nav-links-list">
            <?php foreach ($pts as $post_type) : ?>
                <li>
                    <a href="<?php echo URLROOT . '/' . $post_type->name; ?>"><?php echo $post_type->plural; ?></a>
                </li>
            <?php endforeach; ?>
            <?php if (isset($_SESSION['user_id'])) : ?>
                <li>
                    <a href="<?php echo URLROOT; ?>/logout">Logout</a>
                </li>
            <?php else : ?>
                <li>
                    <a href="<?php echo URLROOT; ?>/register">Register</a>
                </li>
                <li>
                    <a href="<?php echo URLROOT; ?>/login">Login</a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>