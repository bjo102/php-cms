<?php

/**
 * PHP CMS Views: Admin index.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<aside id="admin-index-sidebar-navigation" class="secondary-navigation">
    <nav class="admin-sidebar-links-navigation">
        <ul class="admin-sidebar-links-list">
            <li><a href="<?php echo get_admin_url( '?admin=posts' ); ?>">All Posts</a></li>
            <li><a href="<?php echo get_admin_url( '?admin=posts&post-type=posts' ); ?>">Posts</a></li>
            <li><a href="<?php echo get_admin_url( '?admin=posts&post-type=articles' ); ?>">Articles</a></li>
            <li><a href="<?php echo get_admin_url( '?admin=terms&taxonomy=category' ); ?>">Categories</a></li>
        </ul>
    </nav>
</aside>

<div class="admin-content-container main-content-wrapper">
    <section id="admin-index-main-content-header">
        <h2 id="admin-index-heading admin-heading heading"><?php echo $data['index_heading']; ?></h2>
        <div>
        <?php if( $data['add_new_link'] ) : ?>
            <a href="<?php echo $data['add_new_link']; ?>"><button class="button"><?php echo $data['add_new_text'] ?></button></a>
        <?php endif; ?>
        </div>
    </section>

    <section id="admin-index-items-section">
        <?php $titleProp = $data['title_prop']; foreach( $data['index_items'] as $item ) : ?>
            <article class="admin-index-item">
                <a class="admin-index-item-heading-link" href="<?php echo $data['index_item_edit_link'] . $item->id; ?>">
                    <h3 class="admin-heading heading"><?php echo $item->$titleProp; ?></h3>
                </a>
                <div class="admin-index-item-actions">
                    <a href="<?php echo $data['index_item_edit_link'] . $item->id; ?>"><button class="button">edit</button></a>
                    <a href="<?php echo $data['index_item_delete_link'] . $item->id; ?>"><button class="button">delete</button></a>
                </div>
            </article>
        <?php endforeach; ?>
    </section>
</div>