<?php

/**
 * PHP CMS Views: Admin post form.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<section class="main-content-wrapper">
    <h2><?php echo $data['form-title']; ?></h2>
    <?php echo $data['form']; ?>
    <?php if (in_array($data['form-type'], ['update_post', 'update_term'])) : ?>
        <a class="delete-entry-link button" href="<?php echo $data['delete-link']; ?>">delete</a>
    <?php endif; ?>
</section>