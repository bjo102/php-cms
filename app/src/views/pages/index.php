<?php

/**
 * PHP CMS Views: Default page.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<section class="main-content-wrapper">
    <h1><?php echo $data['title']; ?></h1>
    <p><?php echo $data['description']; ?></p>
    <?php get_posts_archive($data['posts']); ?>
</section>