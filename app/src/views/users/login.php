<?php

/**
 * PHP CMS Views: User login page.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<section class="main-content-wrapper">
    <h2>Login</h2>
    <p>Please fill in your credentials to log in</p>
    <?php echo $data['form']; ?>
    <a href="<?php echo URLROOT; ?>/register">No account? Register</a>
</section>