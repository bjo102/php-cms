<?php

/**
 * PHP CMS Views: User registration page.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Views
 */

?>

<section class="main-content-wrapper">
    <h2>Create An Account</h2>
    <p>Please fill out this form to register with us</p>
    <?php echo $data['form']; ?>
</section>