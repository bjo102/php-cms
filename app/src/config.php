<?php

/**
 * PHP CMS Configuation file.
 * 
 * This contains vital infromation about paths, URLs, API keys and the app version, etc.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Library
 */

/**
 * The current version of this app.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('APPVERSION', '2.0.1');

/**
 * App root path.
 * 
 * This contains the trailing "/" slash.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('APPROOT', getenv('APPROOT'));

/**
 * The name of the website.
 * 
 * This will appear in various places that are used to identify the website.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('SITENAME', getenv('SITENAME'));

/**
 * URL root of the website.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('URLROOT', getenv('URLROOT'));

/**
 * Database host.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('DB_HOST', getenv('DB_HOST'));

/**
 * Database name.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('DB_NAME', getenv('DB_NAME'));

/**
 * Database user.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('DB_USER', getenv('DB_USER'));

/**
 * Database password.
 * 
 * @since 1.0.0
 * 
 * @var string
 */
define('DB_PASS', getenv('DB_PASS'));

/**
 * The API key for TinyMCE.
 * 
 * @since 2.0.0
 * 
 * @link https://www.tiny.cloud
 * @var string
 */
define('TMCE_KEY', getenv('TMCE_KEY'));