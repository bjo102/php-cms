<?php

/**
 * PHP CMS Classes: Form
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Classes
 */

/**
 * The Form Class.
 * 
 * This generates input templates and data values.
 * 
 * @since 2.0.0
 */
class Form
{
    /**
     * The ID attribute of the form.
     * 
     * @since 2.0.0
     * 
     * @var string
     */
    private $form_id;

    /**
     * The ID attribute of the form.
     * 
     * @since 2.0.0
     * 
     * @var string
     */
    private $form_class;

    /**
     * The class attribute of the form.
     * 
     * @since 2.0.0
     * 
     * @var string
     */
    private $method;

    /**
     * The fields of the form.
     * 
     * @since 2.0.1
     * 
     * @var array
     */
    private $fields;

    /**
     * The fieldsets of the form.
     * 
     * @since 2.0.0
     * 
     * @var array
     */
    private $field_sets;

    /**
     * The text content of the form submit button.
     * 
     * @since 2.0.0
     * 
     * @var string
     */
    private $submit_txt;

    public function __construct(array $args = null)
    {
        $this->form_id      = isset($args['form_id']) ? $args['form_id'] : null;
        $this->form_class   = isset($args['class']) ? $args['class'] : null;
        $this->method       = isset($args['method']) ? strval($args['method']) : 'post';
        $this->action       = isset($args['action']) ? strval($args['action']) : URLROOT;
        $this->submit_txt   = isset($args['submit_txt']) ? strval($args['submit_txt']) : 'submit';
        $this->field_sets   = isset($args['field_sets']) ? $args['field_sets'] : [];
    }


    public function get_form_id()
    {
        return $this->form_id;
    }

    public function set_form_id($id)
    {
        $this->form_id = $id;
    }

    public function get_form_class()
    {
        return $this->form_class;
    }

    public function set_form_class($class)
    {
        $this->form_class = $class;
    }

    public function set_submit_text($txt)
    {
        $this->submit_txt = $txt;
    }

    public function get_submit_text()
    {
        return $this->submit_txt;
    }

    public function get_action()
    {
        return $this->action;
    }

    public function set_action($action)
    {
        $this->action = $action;
    }

    public function get_field_sets()
    {
        return $this->field_sets;
    }

    public function set_field_sets($field_sets)
    {
        $this->field_sets = $field_sets;
    }

    public function get_field($name, $field_set)
    {
        return $this->field_sets[$field_set]['fields'][$name];
    }

    public function set_field($name, $field_set, $prop, $value)
    {
        $this->field_sets[$field_set]['fields'][$name][$prop] = $value;
    }

    /**
     * Get the fields of the current form object.
     * 
     * @since 2.0.0
     * 
     * @param boolean $data Whether to include HTML and values.
     * @return array The fields of the current form object.
     */
    public function get_fields()
    {
        $fields = [];
        foreach($this->field_sets as $fieldset) {
            foreach($fieldset['fields'] as $field) {
                $fields[] = $field;
            }
        }
        return $fields;
    }

    /**
     * Get the form as HTML.
     * 
     * @since 2.0.0
     * 
     * @return string The form HTML.
     */
    public function get_form_html()
    {
        $id = isset($this->form_id) ? ' id="' . $this->form_id . '"' : '';
        $class = isset($this->form_class) ? ' ' . $this->form_class . '"' : '"';
        $action = isset($this->action) ? ' action="' . $this->action . '"' : '';
        $method = isset($this->method) ? ' method="' . $this->method . '"' : '';

        $html = '';

        if (!empty($this->field_sets)) :         
            $html .= '<form' . $id . ' class="form' . $class . $action  . $method . '>';
            $html .= '<div class="form-body">';
            foreach ($this->field_sets as $set) :
                $html .= '<fieldset>';
                foreach ($set['fields'] as $key => $field) {
                    $field = $this->process_field($key, $field);
                    $html .= $field['html'];
                }
                $html .= '</fieldset>';
            endforeach;
            $html .= '</div>';

            $html .= '<div class="form-footer">';
            $html .= '<input type="submit" class="button form-submit" value="' . $this->submit_txt . '">';
            $html .= '</div>';
            $html .= '</form>';
        endif;

        return $html;
    }

    /**
     * Set up the given form field with data.
     * 
     * Apply the appropriate label, attributes and input HTML template as properties, based on given data.
     * 
     * @since 2.0.0
     * 
     * @param string $field The input name.
     * @param array $props The field data to apply as content.
     * @return $props The updated data for the given field.
     */
    private function process_field($field, $props)
    {
        if (!isset($props['type'])) {
            print_r($this->error('You must set a type for <strong>' . $field . '</strong>'));
            return;
        }

        if (!isset($props['id'])) {
            $props['id'] = $field;
        }
        if (!isset($props['label'])) {
            $props['label'] = preg_replace('/_/', ' ', $field);
        }
        if (!isset($props['value'])) {
            $props['value'] = '';
        }

        switch ($props['type']) {
            case 'text':
                $props['html'] = $this->get_input_html($field, $props);
                break;
            case 'textarea':
                $props['html'] = $this->get_text_area_html($field, $props);
                break;
            case 'email':
                $props['html'] = $this->get_input_html($field, $props, 'email');
                break;
            case 'checkbox':
                $props['html'] = $this->get_input_html($field, $props, 'checkbox', true);
                break;
            case 'password':
                $props['html'] = $this->get_input_html($field, $props, 'password');
                break;
            case 'file':
                $props['html'] = $this->get_input_html($field, $props, 'file');
                break;
            case 'select':
                $props['html'] = $this->get_select_html($field, $props);
                break;
        };

        if (!empty($props['error'])) {
            $props['html'] .= '<p class="form-field-err">' . $props['error'] . '</p>';
        }

        $props['html'] = $this->get_field_wrapper_html($props);

        return $props;
    }

    /**
     * Generate an input.
     * 
     * @since 2.0.0
     * 
     * @param string $field The input name.
     * @param array $props The field data to apply as content.
     * @param string $type The type of input that is returned.
     * @param boolean $labelAfter Whether the label should come after the the input. This is useful for checkboxes.
     * @return string The input label HTML.
     */
    private function get_input_html(string $field, array $props, string $type = 'text', bool $labelAfter = false)
    {
        if (!isset($field) || !isset($props)) {
            return;
        }

        $html = '<input type="' . $type . '"' . $this->set_field_attributes($field, $props);

        if (isset($props['value'])) $html .= ' value="' . $props['value'] . '"';

        $html .= '>';

        if ($labelAfter == true) {
            $html = $html . $this->get_label_html($field, $props) . '<br>';
        } else {
            $html = $this->get_label_html($field, $props) . $html;
        }

        return $html;
    }

    /**
     * Generate an input label.
     * 
     * @since 2.0.0
     * 
     * @param string $field The input name.
     * @param array $props The field data to apply as content.
     * @return string The input label HTML.
     */
    private function get_label_html($field, $props)
    {
        return '<label for="' . $field . '">' . $props['label'] . '</label>';
    }

    /**
     * Generate a textarea.
     * 
     * @since 2.0.0
     * 
     * @param string $field The input name.
     * @param array $props The field data to apply as content.
     * @return string The textarea HTML.
     */
    private function get_text_area_html($field, $props)
    {
        return $this->get_label_html($field, $props) . ' <textarea' . $this->set_field_attributes($field, $props) . '>' . $props['value'] . '</textarea>';
    }

    /**
     * Generate a selection dropdown.
     * 
     * @since 2.0.0
     * 
     * @param string $field The input name.
     * @param array $props The field data to apply as options.
     * @return string The select dropdown HTML.
     */
    private function get_select_html($field, $props)
    {
        $html =  $this->get_label_html($field, $props) . '<select ' . $this->set_field_attributes($field, $props) . '>';

        $i = 0;
        while ($i < count($props['options'])) {
            $opt = $props['options'][$i];
            $selected = (isset($opt['selected']) && $opt['selected']);
            $html .= '<option value="' . $opt['value'] . '"';
            if ($selected) {
                $html .= ' selected';
            }
            $html .= '>' . $opt['label'] . '</option>';
            $i++;
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * Set the attributes of a given field.
     * 
     * @since 2.0.0
     * 
     * @param string $field The input name.
     * @param array $props The field data to apply as attributes.
     * @return string The input attributes.
     */
    private function set_field_attributes($field, $props)
    {
        $html = '';

        if (isset($props['id'])) {
            $html .= ' id="' . $props['id'] . '"';
        }

        if (!empty($props['class'])) {
            $html .= ' class="' . $props['class'] . '"';
        }

        $html .= ' name="' . $field . '"';

        if (!empty($props['min'])) {
            $html .= ' min="' . $props['min'] . '"';
        }
        if (!empty($props['max'])) {
            $html .= ' max="' . $props['max'] . '"';
        }

        if (!empty($props['pattern'])) {
            $html .= ' pattern="' . $props['pattern'] . '"';
        }

        if (!empty($props['required'])) {
            $html .= ' required';
        }

        return $html;
    }

    /**
     * Generate the input wrapper.
     * 
     * @since 2.0.0
     * 
     * @param array $props The field data to apply as attributes and content. This includes the field itself.
     * @return string The wrapper HTML.
     */
    private function get_field_wrapper_html($props)
    {
        $class = isset($props['container_class']) ? $props['container_class'] : '';
        return '<div class="field-container ' . $props['type'] . '-field ' . $class . '">' . $props['html'] . '</div>';
    }

    /**
     * Echo an error message.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    private function error($msg)
    {
        echo '<p class="form-error">' . $msg . '</p>';
    }
}
