<?php

/**
 * PHP CMS Classes: Controller
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Classes
 */

/**
 * The base controller class.
 * 
 * This provides the basis for other controllers.
 * 
 * @since 1.0.0
 */
class Controller
{

    /**
     * The current model to be used for this controller.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $current_model;

    /**
     * The current model to be used for this controller.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $current_method;

    /**
     * The form object.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $form;

    /**
     * Data for form submissions & post model.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $submission;

    /**
     * The view data.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $vdata = [];

    /**
     * Retrives the existing file in app/models and instantiates the contained model class.
     * 
     * @since 1.0.0
     * 
     * @return object The model.
     */
    public function model($model)
    {
        require_once APPROOT . 'src/models/' . $model . '.php';
        return new $model();
    }

    /**
     * Load the view for the user.
     * 
     * The view is wrapped in other views, depending on what template is chosen.
     * 
     * @since 1.0.0
     * 
     * @param string $view The file to be used as the view.
     * @param array $data The data to be passed into the view.
     * @param string $temp The identifyer for which template to use. Currently uses the terms "def" and "admin".
     * @return void
     */
    public function view($view, $data = [], $temp = 'def')
    {
        $viewsDir = APPROOT . 'src/views/';
        $viewFile = $viewsDir . $view . '.php';
        // Check for view file
        if (file_exists($viewFile)) {
            switch ($temp) {
                case 'def':
                    require $viewsDir . 'inc/public/header.php';
                    require $viewFile;
                    require $viewsDir . 'inc/public/footer.php';
                    break;
                case 'admin':
                    require $viewsDir . 'inc/admin/header.php';
                    require $viewFile;
                    require $viewsDir . 'inc/admin/footer.php';
                    break;
                default:
                    require $viewsDir . 'inc/public/header.php';
                    require $viewFile;
                    require $viewsDir . 'inc/public/footer.php';
                    break;
            }
        } else {
            die($viewFile . ': View does not exist');
        }
    }
}
