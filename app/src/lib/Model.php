<?php

/**
 * PHP CMS Classes: Model
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Classes
 */

/**
 * The base class for models.
 * 
 * @since 2.0.0
 */
class Model
{
    /**
     * The database object for performing CRUD operations on the database.
     * 
     * @since 1.0.0
     * 
     * @var object
     */
    protected $db;

    public function __construct()
    {
        $this->db = new Database;
    }
}
