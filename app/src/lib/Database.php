<?php

/**
 * PHP CMS Classes: Database
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Classes
 */

/**
 * Provide the app with database CRUD capabilities.
 *
 * Tasks include:
 * 
 * - PDO Database Class
 * - Connect to database
 * - Create prepared statements
 * - Bind values
 * - Return rows and results
 * 
 * @since 1.0.0
 */
class Database
{

    /**
     * The database hostname.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    private $host = DB_HOST;

    /**
     * The database username.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    private $user = DB_USER;

    /**
     * The database password.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    private $pass = DB_PASS;

    /**
     * The database name.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    private $dbname = DB_NAME;

    /**
     * The "PHP Data Objects" instance for the database.
     * 
     * @since 1.0.0
     * 
     * @link https://www.php.net/manual/en/book.pdo.php
     * @var object
     */
    private $dbh;

    /**
     * The database statement.
     * 
     * @since 1.0.0
     * 
     * @var 
     */
    private $stmt;

    /**
     * The database error.
     * 
     * @since 1.0.0
     * 
     * @var 
     */
    private $error;

    /**
     * Set the data source name and attempt a connection to the database.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function __construct()
    {
        // Set DSN
        $dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;
        $options = array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        );

        // Create PDO instance
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        } catch (PDOException $e) {
            echo '<strong>Database failure:</strong><br> ';
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

    /**
     * Prepare statement for the database.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function query($sql)
    {
        $this->stmt = $this->dbh->prepare($sql);
    }

    /**
     * Bind values to parameters by type.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function bind($param, $value, $type = null)
    {
        if (is_null($type)) {
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }

        $this->stmt->bindValue($param, $value, $type);
    }

    /**
     * Execute the prepared statement.
     * 
     * @since 1.0.0
     * 
     * @return boolean
     */
    public function execute()
    {
        return $this->stmt->execute();
    }

    /**
     * Get a set of results as an array of objects.
     * 
     * @since 1.0.0
     * 
     * @return array
     */
    public function resultSet()
    {
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Get a single record as an object.
     * 
     * @since 1.0.0
     * 
     * @return object|false
     */
    public function single()
    {
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Get row count of a table.
     * 
     * @since 1.0.0
     * 
     * @return int
     */
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}
