<?php

/**
 * PHP CMS Controllers: Admin_Controller
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Controllers
 */

 /**
  * The base controller class for admin controllers.
  * 
  * @since 2.0.0
  */
class Admin_Controller extends Controller
{
    /**
     * The URL to use as a base for admin query variables.
     * 
     * @since 2.0.0
     * 
     * @var array
     */
    protected $admin_base_url;

    public function __construct()
    {
        if (is_logged_in() == false) {
            redirect('login');
        }

        /* if (!isset($_GET['admin']) || empty($_GET['admin'])) {
            redirect();
        } */
    }

    protected function set_form_args()
    {
        // submission defaults
        $this->submission = [
            'title'         => trim($_POST['post-title']),
            'name'          => slugify_string($_POST['post-name']),
            'excerpt'       => trim($_POST['post-excerpt']),
            'content'       => trim($_POST['post-content']),
            'author'        => $_SESSION['user_id'],
            'title_err'     => '',
            'name_err'      => '',
            'content_err'   => ''
        ];
    }
}
