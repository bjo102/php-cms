<?php

/**
 * PHP CMS Classes: Init
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Classes
 */

/**
 * The users primary gateway to the app. 
 *
 * This uses the URL segments and query variables to provide the appropriate controller(s). The URL is used as follows:
 * 
 * - The first URL segment is used as the **controller**.
 * - The second URL segment is used as the controller **method**.
 * - The segments beyond the second are used as controller method **arguements**.
 * 
 * @since 1.0.0
 */
class Init
{

    /**
     * The current URL.
     * 
     * @since 1.0.0
     * 
     * @var string|array
     */
    private $url;

    /**
     * The controller for Pages.
     * 
     * This is used to get the post type model, and to provide methods for which page to show.
     * 
     * @since 1.0.0
     * 
     * @var object
     */
    private $pages_controller;

    /**
     * The controller for Users.
     * 
     * @since 1.0.0
     * 
     * @var object
     */
    private $users_controller;

    /**
     * The post type model.
     * 
     * This is used to define which post type archive to show.
     * 
     * @since 1.0.0
     * 
     * @var object
     */
    private $pt_model;

    /**
     * The currently selected controller to be used by the end user.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    private $current_controller = 'Pages';

    /**
     * The currently selected method of the selected controller to be called.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    private $current_method = 'index';

    /**
     * The possible arguements to be passed into currently selected method.
     * 
     * @since 1.0.0
     * 
     * @var string
     */
    private $params = [];

    public function __construct()
    {
        $this->pages_controller = new Pages;
        $this->pt_model = $this->pages_controller->get_pt_model();
        $this->url = get_url(true);
        $this->set_controller();
        $this->call_controller_method();
    }

    /**
     * Set the controller to be used by the user.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function set_controller()
    {
        $user_pages = ['login', 'logout', 'register'];

        if (isset($this->url[0])) :
            if ($this->url[0] == 'admin') {
                $admin_var = isset($_GET['admin']) ? $_GET['admin']: 'posts';
                switch ($admin_var) {
                    case 'posts':
                        $this->current_controller = 'Posts_Admin';
                        break;
                    case 'terms':
                        $this->current_controller = 'Terms_Admin';
                        break;
                    default:
                        $this->current_controller = 'Posts_Admin';
                        break;
                }

                // pages query
            } else if (method_exists($this->pages_controller, $this->url[0])) {
                $this->current_controller = 'Pages';
            } else if (in_array($this->url[0], $user_pages)) {
                $this->current_controller = 'Users_Public';
                // post type query - is url segment a post type?
            } else if ($this->pt_model->get_post_type($this->url[0], 'name')) {
                $this->current_controller = 'Posts_Public';
            }

        endif;

        // load controller
        $this->current_controller = new $this->current_controller;
    }

    /**
     * Call the method to be called for the selected controller.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function call_controller_method()
    {
        if (isset($this->url[0])) {
            if (method_exists($this->current_controller, $this->url[0]) && is_callable([$this->current_controller, $this->url[0]])) {
                $this->current_method = $this->url[0];
                unset($this->url[0]);
            }
        }

        $this->params = $this->url ? array_values($this->url) : [];
        call_user_func_array([$this->current_controller, $this->current_method], $this->params);
    }
}
