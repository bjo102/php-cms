<?php

/**
 * PHP CMS Models: Post
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Models
 */

/**
 * The posts model.
 * 
 * @since 1.0.0
 */
class Post extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a post from the database.
     * 
     * @since 1.0.0
     * 
     * @param mixed $val The value to search for.
     * @param string $by The column on the database table to search through.
     * @param array $args The values to amend the SQL query.
     * @return object The post as a database row object.
     */
    public function get_post($val, $by = 'id', $args = array())
    {
        // sanitize search value
        $by = strval($by);

        // whitelist search values
        $w_list = ['id', 'name'];
        if (!in_array($by, $w_list)) return;

        // default bind items
        $bind_vals = array(
            $by => $val
        );

        // default sql statement
        $sql_str = 'SELECT * FROM posts WHERE ' . $by . ' = :' . $by;

        // post type id
        if (isset($args['post_type'])) {
            $sql_str .= ' AND post_type = :post_type';
            $bind_vals['post_type'] = intval($args['post_type']);
        }

        // prepare sql statement
        $this->db->query($sql_str);

        // loop through bind key value pairs
        foreach ($bind_vals as $placeholder => $value) {
            $this->db->bind(':' . $placeholder, $value);
        }

        $row = $this->db->single();

        return $row;
    }

    /**
     * Get a list of posts from the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The values to amend to the SQL query.
     * @return array The posts as database row objects.
     */
    public function get_posts(array $args = array())
    {
        // default bind items
        $bind_vals = array();

        // default sql statement
        $sql_str = 'SELECT *,
            posts.id as id,
            posts.title as title,
            posts.name as name,
            posts.published as published,
            posts.term as term,
            post_types.name as postTypeName,
            post_types.single as postTypeSingle,
            users.id as userId,
            users.created as userCreated
            FROM posts
            INNER JOIN users ON posts.author = users.id
            INNER JOIN terms ON posts.term = terms.id
            INNER JOIN post_types ON posts.post_type = post_types.id';

        // apply post type
        if (isset($args['post_type'])) {
            $sql_str .= ' WHERE post_type = :post_type';
            $bind_vals['post_type'] = intval($args['post_type']);
        }

        $sql_str .= ' ORDER BY posts.published DESC';

        $this->db->query($sql_str);

        // loop through bind key value pairs
        foreach ($bind_vals as $placeholder => $value) {
            $this->db->bind(':' . $placeholder, $value);
        }

        $results = $this->db->resultSet();

        return $results;
    }

    /**
     * Add a post to the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The data used to populate the post columns.
     * @return boolean The query execution status.
     */
    public function add_post($data)
    {
        $this->db->query(
            'INSERT
            INTO posts (title, name, author, content, excerpt, post_type, term)
            VALUES(:title, :name, :author, :content, :excerpt, :post_type, :term)'
        );
        $this->db->bind(':title',   $data['title']);
        $this->db->bind(':name',    $data['name']);
        $this->db->bind(':excerpt', $data['excerpt']);
        $this->db->bind(':author',  $data['author']);
        $this->db->bind(':content', $data['content']);
        $this->db->bind(':post_type', $data['post_type']);
        $this->db->bind(':term', $data['term']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update a post on the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The data used to update the post columns.
     * @return boolean The query execution status.
     */
    public function update_post($data)
    {
        $this->db->query(
            'UPDATE posts
            SET title = :title, name = :name, excerpt = :excerpt, content = :content, term = :term
            WHERE id = :id'
        );
        $this->db->bind(':title', $data['title']);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':excerpt', $data['excerpt']);
        $this->db->bind(':content', $data['content']);
        $this->db->bind(':term', $data['term']);
        $this->db->bind(':id', $data['id']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete a post from the database.
     * 
     * @since 1.0.0
     * 
     * @param string|int $id The ID of the post to delete.
     * @return boolean The query execution status.
     */
    public function delete_post($id)
    {
        $this->db->query('DELETE FROM posts WHERE id = :id');
        $this->db->bind(':id', $id);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
