<?php

/**
 * PHP CMS Models: User
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Models
 */

/**
 * The user model.
 * 
 * @since 1.0.0
 */
class User extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Regsiter a new user.
     * 
     * @since 1.0.0
     * 
     * @param array $data The data used to populate the user columns.
     * @return boolean The query execution status.
     */
    public function register($data)
    {
        $this->db->query('INSERT INTO users (name, email, password) VALUES(:name, :email, :password)');
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);

        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Verify a user login.
     * 
     * @since 1.0.0
     * 
     * @param string $email The email address to check.
     * @param string $password The password to check.
     * @return object|false The user as a database row object, or false on failure.
     */
    public function login($email, $password)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        $hashed_password = $row->password;
        if (password_verify($password, $hashed_password)) {
            return $row;
        } else {
            return false;
        }
    }

    /**
     * Verify a users existence by email.
     * 
     * @since 1.0.0
     * 
     * @param string $email The email address to check.
     * @return boolean The database table row count status.
     */
    public function findUserByEmail($email)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        // Bind value
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        if ($this->db->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get a user from the database.
     * 
     * @since 1.0.0
     * 
     * @param string|int $id The ID of the user to search for.
     * @return object The user as a database row object.
     */
    public function getUserById($id)
    {
        $this->db->query('SELECT * FROM users WHERE id = :id');
        // Bind value
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }
}
