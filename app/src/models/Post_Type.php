<?php

/**
 * PHP CMS Models: Post_Type
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Models
 */

/**
 * The post type model.
 * 
 * @since 2.0.0
 */
class Post_Type extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a post type from the database.
     * 
     * @since 1.0.0
     * 
     * @param mixed $val The value to search for.
     * @param string $by The column on the database table to search through.
     * @return object The post type as a database row object.
     */
    public function get_post_type($val, $by = 'id')
    {
        $this->db->query('SELECT * FROM post_types WHERE ' . $by . ' = :' . $by);
        $this->db->bind(':' . $by, $val);

        $row = $this->db->single();
        return $row;
    }

    /**
     * Get a list of post types from the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The values to amend to the SQL query.
     * @return array The post types as database row objects.
     */
    public function get_post_types(array $args = array())
    {
        $bind_vals = array();

        $sql_str = 'SELECT';

        $sql_str .= ' *,
        post_types.id as postTypeId,
        post_types.single as postTypeSingle,
        post_types.plural as postTypePlural,
        post_types.name as postTypeName,
        post_types.description as postTypeDescription
        FROM post_types
        ORDER BY post_types.name ASC';

        if (isset($args['limit']) && is_int($args['limit'])) {
            $sql_str .= ' LIMIT :limit';
            $bind_vals['limit'] = intval($args['limit']);
        }

        $this->db->query($sql_str);

        // loop through bind key value pairs
        foreach ($bind_vals as $placeholder => $value) {
            $this->db->bind(':' . $placeholder, $value);
        }

        /*$this->db->query(
            'SELECT *,
            post_types.id as postTypeId,
            post_types.single as postTypeSingle,
            post_types.plural as postTypePlural,
            post_type.name as postTypeName,
            post_type.description as postTypeDescription
            FROM post_types
            ORDER BY post_types.name ASC'
        );*/

        $results = $this->db->resultSet();

        return $results;
    }
}
