<?php

/**
 * PHP CMS Models: Term
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Models
 */

/**
 * The taxonomy terms model.
 * 
 * @since 1.0.0
 */
class Term extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a list of terms from the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The values to amend to the SQL query.
     * @return array The terms as database row objects.
     */
    public function get_terms(array $args = array())
    {
        // default bind items
        $bind_vals = array();

        // default sql statement
        $sql_str = 'SELECT *,
            terms.id as id,
            terms.title as title,
            terms.name as name
            FROM terms
            INNER JOIN taxonomies ON terms.taxonomy = taxonomies.id';

        // apply post type
        if (isset($args['taxonomy'])) {
            $sql_str .= ' WHERE taxonomy = :taxonomy';
            $bind_vals['taxonomy'] = intval($args['taxonomy']);
        }

        $sql_str .= ' ORDER BY terms.title DESC';

        $this->db->query($sql_str);

        // loop through bind key value pairs
        foreach ($bind_vals as $placeholder => $value) {
            $this->db->bind(':' . $placeholder, $value);
        }

        $results = $this->db->resultSet();

        return $results;
    }

    /**
     * Get a term from the database.
     * 
     * @since 1.0.0
     * 
     * @param mixed $val The value to search for.
     * @param string $by The column on the database table to search through.
     * @param array $args The values to amend the SQL query.
     * @return object The term as a database row object.
     */
    public function get_term($val, $by = 'id', $args = array())
    {
        // sanitize search value
        $by = strval($by);

        // whitelist search values
        $w_list = ['id', 'name'];
        if (!in_array($by, $w_list)) return;

        // default bind items
        $bind_vals = array(
            $by => $val
        );

        // default sql statement
        $sql_str = 'SELECT * FROM terms WHERE ' . $by . ' = :' . $by;

        // post type id
        /* if( isset( $args['post_type'] ) ) {
            $sql_str .= ' AND post_type = :post_type';
            $bind_vals['post_type'] = intval( $args['post_type'] );
        } */

        // prepare sql statement
        $this->db->query($sql_str);

        // loop through bind key value pairs
        foreach ($bind_vals as $placeholder => $value) {
            $this->db->bind(':' . $placeholder, $value);
        }

        $row = $this->db->single();

        return $row;
    }

    /**
     * Add a term to the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The data used to populate the term columns.
     * @return boolean The query execution status.
     */
    public function add_term($data)
    {
        $this->db->query(
            'INSERT
            INTO terms (title, name, taxonomy)
            VALUES(:title, :name, :taxonomy)'
        );
        $this->db->bind(':title',   $data['title']);
        $this->db->bind(':name',    $data['name']);
        $this->db->bind(':taxonomy', $data['taxonomy']);

        if ($this->db->execute()) return true;
        else return false;
    }

    /**
     * Update a term on the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The data used to update the term columns.
     * @return boolean The query execution status.
     */
    public function update_term($data)
    {
        $this->db->query(
            'UPDATE terms
            SET title = :title, name = :name
            WHERE id = :id'
        );

        $this->db->bind(':title', $data['title']);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':id', $data['id']);

        if ($this->db->execute()) return true;
        else return false;
    }

    /**
     * Delete a term from the database.
     * 
     * @since 1.0.0
     * 
     * @param string|int $id The ID of the term to delete.
     * @return boolean The query execution status.
     */
    public function delete_term($id)
    {
        $this->db->query('DELETE FROM terms WHERE id = :id');
        $this->db->bind(':id', $id);

        if ($this->db->execute()) return true;
        else return false;
    }
}
