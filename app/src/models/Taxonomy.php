<?php

/**
 * PHP CMS Models: Taxonomy
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Models
 */

/**
 * The taxonomy model.
 * 
 * @since 1.0.0
 */
class Taxonomy extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get a taxonomy from the database.
     * 
     * @since 1.0.0
     * 
     * @param mixed $val The value to search for.
     * @param string $by The column on the database table to search through.
     * @return object The taxonomy as a database row object.
     */
    public function get_taxonomy($val, $by = 'id')
    {
        $this->db->query('SELECT * FROM taxonomies WHERE ' . $by . ' = :' . $by);
        $this->db->bind(':' . $by, $val);

        $row = $this->db->single();
        return $row;
    }

    /**
     * Get a list of taxonomies from the database.
     * 
     * @since 1.0.0
     * 
     * @param array $args The values to amend to the SQL query.
     * @return array The taxonomies as database row objects.
     */
    public function get_taxonomies(array $args = array())
    {
        // default bind items
        $bind_vals = array();

        $sql_str = 'SELECT';

        $sql_str .= ' *
        FROM taxonomies
        ORDER BY taxonomies.name ASC';

        if (isset($args['limit']) && is_int($args['limit'])) {
            $sql_str .= ' LIMIT :limit';
            $bind_vals['limit'] = intval($args['limit']);
        }

        $this->db->query($sql_str);

        // loop through bind key value pairs
        foreach ($bind_vals as $placeholder => $value) {
            $this->db->bind(':' . $placeholder, $value);
        }

        $results = $this->db->resultSet();

        return $results;
    }
}
