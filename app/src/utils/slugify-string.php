<?php

/**
 * PHP CMS functions: slugify_string.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Make a string URL friendly.
 * 
 * @since 2.0.0
 * 
 * @param string $str The string to slugify_string.
 * @return string The slugified string.
 */
function slugify_string(string $str) : string
{
    $str = preg_replace('/^\s+|\s+$/m', '', $str);
    $str = strtolower($str);

    // remove accents, swap ñ for n, etc
    $accentsAlt = array(
        'à' => 'a',
        'á' => 'a',
        'ä' => 'a',
        'â' => 'a',
        'è' => 'e',
        'é' => 'e',
        'ë' => 'e',
        'ê' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'ï' => 'i',
        'î' => 'i',
        'ò' => 'o',
        'ó' => 'o',
        'ö' => 'o',
        'ô' => 'o',
        'ù' => 'u',
        'ú' => 'u',
        'ü' => 'u',
        'û' => 'u',
        'ñ' => 'n',
        'ç' => 'c',
        '·' => '-',
        '/' => '-',
        '_' => '-',
        ',' => '-',
        ':' => '-',
        ';' => '-',
    );

    $str = strtr($str, $accentsAlt); // no more accents.
    $str = preg_replace('/[^a-z0-9 -]/m', '', $str); // remove invalid chars
    $str = preg_replace('/\s+/m', '-', $str); // collapse whitespace and replace by -
    $str = preg_replace('/-+/m', '-', $str); // collapse dashes
    $str = preg_replace('/^-+/', '', $str); // trim - from start of text
    $str = preg_replace('/-+$/', '', $str); // trim - from end of text

    return $str;
}
