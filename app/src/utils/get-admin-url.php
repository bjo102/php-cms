<?php

/**
 * PHP CMS functions: get_admin_url.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Get the URL for the admin dashboard.
 * 
 * @since 2.0.0
 * 
 * @param string $after The string to append to the returned URL.
 * @return string The admin dashboard URL.
 */
function get_admin_url(string $after = '')
{
    $url = URLROOT . '/admin/' . $after;
    return $url;
}