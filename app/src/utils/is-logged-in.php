<?php

/**
 * PHP CMS functions: is_logged_in.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Check if the user is logged in.
 * 
 * @since 1.0.0
 * 
 * @return boolean
 */
function is_logged_in()
{
    if (isset($_SESSION['user_id'])) {
        return true;
    } else {
        return false;
    }
}