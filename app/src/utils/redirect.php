<?php

/**
 * PHP CMS functions: redirect.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Redirect to a new page.
 * 
 * This uses the header() function, when printing text before using this function, you might get the following error:
 * 
 * - "PHP error: Cannot modify header information – headers already sent"
 * 
 * So make sure there is no text being printed before using this.
 * 
 * @since 1.0.0
 * @since 2.0.0 Flush headers and exit script.
 * 
 * @param string $page The page to redirect to. Include url segments.
 * @link https://stackoverflow.com/questions/1793482/php-error-cannot-modify-header-information-headers-already-sent
 * @return void
 */
function redirect(string $page = ''): void
{
    ob_start();
    header('location: ' . URLROOT . '/' . $page);
    ob_end_flush();
    exit();
}
