<?php

/**
 * PHP CMS functions: get_post_link.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Get the URL link to a given post.
 * 
 * @since 2.0.0
 * 
 * @param object $post The post to get the link for.
 * @param boolean $admin Whether this is a link to the admin editor for this post. Defaults to false.
 * @return string the link. 
 */
function get_post_link($post, $admin = false)
{
    $base = $admin == true ? '/admin/?post=edit&post_id=' : '/' . $post->postTypeName . '/';
    $id = $admin == true ? $post->postId : $post->postName;
    return URLROOT . $base . $id;
};