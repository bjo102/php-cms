<?php

/**
 * PHP CMS functions: get_url.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Get a list of articles form a given array of posts.
 * 
 * This loads the posts archive view file.
 * 
 * @since 2.0.0
 * 
 * @param array $posts The posts to populate the archive with.
 * @return void
 */
function get_posts_archive(array $posts)
{
    require_once APPROOT . 'src/views/posts/archive.php';
}