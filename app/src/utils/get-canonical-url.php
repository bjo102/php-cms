<?php

/**
 * PHP CMS functions: get_canonical_url.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Get the canonical URL for the website.
 * 
 * @since 2.0.0
 * 
 * @return string The cononical URL.
 */
function get_canonical_url()
{
    $url = get_url();
    $parsed = parse_url($url);
    $cononical = ($parsed['path'] != '') ? URLROOT . '/' . $parsed['path'] . '/' : URLROOT . '/';
    return $cononical;
}