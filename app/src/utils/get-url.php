<?php

/**
 * PHP CMS functions: get_url.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Functions
 */

/**
 * Get the current URL in the browser.
 * 
 * @since 1.0.0
 * 
 * @param boolean $split Whether to split the URL into segments. Defaults to false.
 * @return string|array
 */
function get_url(bool $split = false)
{
    if (isset($_GET['url'])) {
        $url = rtrim($_GET['url'], '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);

        if ($split == true) {
            $url = explode('/', $url);
        }

        return $url;
    }
    return NULL;
};