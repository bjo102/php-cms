<?php

/**
 * PHP CMS Controllers: Pages
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Controllers
 */

/**
 * The Pages controller class.
 * 
 * @since 2.0.0
 */
class Pages extends Controller
{

    /**
     * The post types model for this controller.
     *  
     * @since 2.0.0
     * 
     * @var object
     */
    private $pt_model;

    public function __construct()
    {
        $this->current_model = $this->model('Post');
        $this->pt_model = $this->model('Post_Type');
    }

    public function get_pt_model()
    {
        return $this->pt_model;
    }

    /**
     * The default page.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function index()
    {
        $postsList = $this->current_model->get_posts();
        
        $data = [
            'title' => 'Home Page Archive',
            'description' => '',
            'posts' => $postsList
        ];

        $this->view('pages/index', $data);
    }

    /**
     * The "About me" page.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function about()
    {
        $data = [
            'title' => 'Byron O\'Malley',
            'description' => 'App to share posts with other users'
        ];

        $this->view('pages/about', $data);
    }
}
