<?php

/**
 * PHP CMS Controllers: Posts_Admin
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Controllers
 */

 /**
  * The posts admin controller class.
  * 
  * @since 2.0.0
  */
class Posts_Admin extends Admin_Controller
{
    /**
     * The current post type model.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $pt_model;

    /**
     * The current taxonomy terms model.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $terms_model;

    /**
     * The current post to get data from.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $current_post;

    /**
     * The current post type to get data from.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $current_post_type;

    /**
     * The field set for the admin post editing form.
     * 
     * @since 2.0.0
     * 
     * @var array
     */
    public $post_edit_fieldset = array(
        'post_title' => [
            'type' => 'text',
            'label' => 'post name',
            'container_class' => 'field-width-50',
            'required' => true,
            'min' => 1,
            'max' => 100,
            'sql_val' => '',
        ],
        'post_name' => [
            'type' => 'text',
            'label' => 'post name',
            'container_class' => 'field-width-50',
            'min' => 1,
            'max' => 100,
        ],
        'post_excerpt' => [
            'type' => 'textarea',
            'max' => 500,
        ],
        'post_content' => [
            'type' => 'textarea',
            'class' => 'mceEditorField',
        ],
        'category' => [
            'type' => 'select',
            'options' => [],
            'container_class' => 'field-width-30',
        ]
    );

    public function __construct()
    {
        parent::__construct();
        $this->current_model    = $this->model('Post');
        $this->admin_base_url   = URLROOT . '/admin/?admin=post';
        $this->pt_model         = $this->model('Post_Type');
        $this->terms_model      = $this->model('Term');
    }

    /**
     * Load the admin index or edit form, based on the current URL.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    public function index()
    {
        if (!isset($_GET['post'])) {
            $this->get_index();
            exit();
        }

        $is_editor      = in_array($_GET['post'], ['edit', 'add']);
        $is_deletion    = $_GET['post'] == 'delete' && isset($_GET['post_id']) && $this->current_model->get_post($_GET['post_id']);

        if ($is_editor) {
            $this->get_edit_form();
            exit();
        }

        if ($is_deletion) {
            $this->current_model->delete_post($_GET['post_id']);
        }
            
        $this->get_index();
    }

    /**
     * Load the primary index view of the admin dashboard.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    private function get_index()
    {
        $args = [];
        $defaultPt = $this->pt_model->get_post_types(['limit' => 1])[0];
        $is_post_type_set = (isset($_GET['post-type']) && $this->pt_model->get_post_type($_GET['post-type'], 'name'));
        $this->vdata['default_post_type'] = $defaultPt->id;
        $this->vdata['title_prop'] = 'title';

        if ($is_post_type_set) :

            $pt = $this->pt_model->get_post_type($_GET['post-type'], 'name');
            $args['post_type'] = intval($pt->id);
            $this->vdata['index_heading'] = $pt->plural;
            $this->vdata['add_new_link'] = $this->admin_base_url . '&post=add&post_type=' . $pt->id;
            $this->vdata['add_new_text'] = 'add new ' . $pt->single;

        else :

            $this->vdata['index_heading'] = 'All Posts';
            $this->vdata['add_new_link'] = $this->admin_base_url . '&post=add&post_type=' . $defaultPt->id;
            $this->vdata['add_new_text'] = 'add new ' . $defaultPt->single;

        endif;

        $this->vdata['index_items'] = $this->current_model->get_posts($args);
        $this->vdata['index_item_edit_link'] = $this->admin_base_url . '&post=edit&post_id=';
        $this->vdata['index_item_delete_link'] = $this->admin_base_url . '&post=delete&post_id=';

        $this->view('admin/index', $this->vdata, 'admin');
    }

    /**
     * Load the view for editing single posts with the controller form.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    private function get_edit_form()
    {
        $is_existing_post = ($_GET['post'] == 'edit' && $this->current_model->get_post($_GET['post_id']));
        $is_new_post = ($_GET['post'] == 'add' && $this->pt_model->get_post_type($_GET['post_type']));
        $is_submission = $_SERVER['REQUEST_METHOD'] == 'POST';
        $form_args = array(
            'form_id' => 'post-edit-form',
            'method' => 'post',
            'field_sets' => [
                'post-edit-fieldset' => [
                    'id' => 'post-edit-fieldset',
                    'fields' => $this->post_edit_fieldset
                ]
            ]
        );
        $this->form = new Form($form_args);

        if ($is_existing_post) :

            $this->current_method = 'update_post';
            $this->current_post = $this->current_model->get_post($_GET['post_id']);
            $this->current_post_type = $this->pt_model->get_post_type($this->current_post->post_type);
            $this->vdata['form-title'] = 'Edit ' . $this->current_post_type->single . ': <span id="admin-form-post-title">' . $this->current_post->title . '</span>';
            $this->vdata['delete-link'] = URLROOT . '/admin/&post=delete&post_id=' . $_GET['post_id'];
            $this->form->set_form_class('form-edit');
            $this->form->set_action(URLROOT . '/admin/?post=edit&post_id=' . $this->current_post->id);
            $this->form->set_submit_text('update');
            $this->form->set_field('post_title', 'post-edit-fieldset', 'value', $this->current_post->title);
            $this->form->set_field('category', 'post-edit-fieldset', 'options', $this->get_terms_for_edit_form($this->current_post->term));
            $this->form->set_field('post_name', 'post-edit-fieldset', 'value', $this->current_post->name);
            $this->form->set_field('post_excerpt', 'post-edit-fieldset', 'value', $this->current_post->excerpt);
            $this->form->set_field('post_content', 'post-edit-fieldset', 'value', $this->current_post->content);

        elseif ($is_new_post) :

            $this->current_method = 'add_post';
            $this->current_post_type = $this->pt_model->get_post_type($_GET['post_type']);
            $this->vdata['form-title'] = 'New ' . $this->current_post_type->single;
            $this->form->set_form_class('form-add-new');
            $this->form->set_action(URLROOT . '/admin/?post=add&post_type=' . $this->current_post_type->id);
            $this->form->set_submit_text('add new');
            $this->form->set_field('category', 'post-edit-fieldset', 'options', $this->get_terms_for_edit_form());

        endif;
        
        if ($is_submission) {
            $this->process_post();
        }

        $this->vdata['form-type'] = $this->current_method;
        $this->vdata['form'] = $this->form->get_form_html();
        $this->view('admin/post', $this->vdata, 'admin');
    }

    /**
     * Handle submissions form the controller form.
     * 
     * Sanitize & validate post data for submission to the controller model.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    private function process_post()
    {
        $this->submission = [
            'id'            => null,
            'title'         => trim($_POST['post_title']),
            'name'          => slugify_string($_POST['post_name']),
            'excerpt'       => trim($_POST['post_excerpt']),
            'content'       => trim($_POST['post_content']),
            'post_type'     => $this->current_post_type->id,
            'term'          => intval($_POST['category']),
            'author'        => $_SESSION['user_id'],
            'title_err'     => '',
            'name_err'      => '',
            'content_err'   => ''
        ];

        if ($this->current_method == 'update_post') {
            $this->submission['id'] = $this->current_post->id;
        }

        if (empty($this->submission['title'])) {
            $this->submission['title_err'] = 'Please enter title';
        }

        if (empty($this->submission['name'])) {
            $this->submission['name'] = slugify_string($this->submission['title']);
        }

        if (empty($this->submission['content'])) {
            $this->submission['content_err'] = 'Please enter content';
        }

        $named_post = $this->current_model->get_post($this->submission['name'], 'name');
        $duplicate_names_check = (
            ($this->current_method == 'add_post' && $named_post) ||
            ($this->current_method == 'update_post' && $named_post && $named_post->id !== $this->submission['id'])
        );

        if ($duplicate_names_check) {
            $this->submission['name_err'] = 'There is already a post with the name "' . $this->submission['name'] . '"';
        }

        $valid_submission = (empty($this->submission['title_err']) && empty($this->submission['name_err']) && empty($this->submission['content_err']));
        
        if (!$valid_submission) {
            $this->form->set_field('post_title', 'post-edit-fieldset', 'value', $this->submission['title']);
            $this->form->set_field('post_title', 'post-edit-fieldset', 'error', $this->submission['title_err']);
            $this->form->set_field('post_name', 'post-edit-fieldset', 'value', $this->submission['name']);
            $this->form->set_field('post_name', 'post-edit-fieldset', 'error', $this->submission['name_err']);
            $this->form->set_field('post_excerpt', 'post-edit-fieldset', 'value', $this->submission['excerpt']);
            $this->form->set_field('post_content', 'post-edit-fieldset', 'value', $this->submission['content']);
            $this->form->set_field('post_content', 'post-edit-fieldset', 'error', $this->submission['content_err']);
            $this->form->set_field('category', 'post-edit-fieldset', 'options', $this->get_terms_for_edit_form());

            return;
        }

        $submission = call_user_func_array([$this->current_model, $this->current_method], [$this->submission]);
        
        if ($submission) {
            if ($this->current_method == 'add_post') {
                $this->current_post = $this->current_model->get_post(
                    $this->submission['name'],
                    'name',
                    ['post_type' => $this->current_post_type->id]
                );
            }
            flash('post_message', 'Post Added');
            redirect('admin?post=edit&post_id=' . $this->current_post->id);
        } else {
            die('Post was not added.');
        }
    }

    /**
     * Get term select options for the post editing screen.
     * 
     * @since 2.0.1
     * 
     * @param int $id The selected option. This is for posts that already exist.
     * @return array The term options.
     */
    private function get_terms_for_edit_form(int $id = null)
    {
        $options = [['value' => null, 'label' => '']];
            
        foreach ($this->terms_model->get_terms(['taxonomy' => 1]) as $category) {
            $newOpt = [
                'value' => $category->id,
                'label' => $category->title
            ];
            if (null != $id && $category->id == $id) {
                $newOpt['selected'] = true;
            }
            $options[] = $newOpt;
        }

        return $options;
    }
}
