<?php

/**
 * PHP CMS Controllers: Users_Public
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Controllers
 */

/**
 * The Users controller class.
 * 
 * @since 2.0.0
 */
class Users_Public extends Controller
{

    /**
     * The form specifications for the user login page.
     * 
     * @since 2.0.0
     * 
     * @var array
     */
    protected $loginFormFields = array(
        'email' => [
            'type'      => 'email',
            'label'     => 'Email',
            'required'  => true,
            'pattern'   => '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$',
            'min'       => 1,
            'max'       => 100,
        ],
        'password' => [
            'type'      => 'password',
            'label'     => 'Password',
            'required'  => true,
            'min'       => 1,
            'max'       => 100,
        ],
    );

    /**
     * The form specifications for the user registration page.
     * 
     * @since 2.0.0
     * 
     * @var array
     */
    protected $registerFormFields = array(
        'name' => [
            'type' => 'text',
            'label' => 'name',
            'required' => true,
            'container_class' => 'field-width-50',
            'min' => 1,
            'max' => 100,
        ],
        'email' => [
            'type' => 'email',
            'label' => 'email',
            'container_class' => 'field-width-50',
            'required' => true,
            'min' => 1,
            'max' => 100,
        ],
        'password' => [
            'type' => 'password',
            'label' => 'password',
            'required' => true,
            'min' => 1,
            'max' => 100,
        ],
        'confirm_password' => [
            'type' => 'password',
            'label' => 'confirm password',
            'required' => true,
            'min' => 1,
            'max' => 100,
        ],
    );

    public function __construct()
    {
        $this->current_model = $this->model('User');
        $this->form = new Form();
    }

    /**
     * Manage user registration.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function register()
    {
        $fieldsets = [
            'register' => [
                'id' => 'registration-fields',
                'fields' => $this->registerFormFields
            ]
        ];
        $this->form->set_field_sets($fieldsets);
        $this->form->set_submit_text('Register');
        $this->form->set_action(URLROOT . '/register');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Init data
            $data = [
                'name' => trim($_POST['name']),
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'confirm_password' => trim($_POST['confirm_password']),
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];

            // Validate Email
            if (empty($data['email'])) {
                $data['email_err'] = 'Pleae enter email';
            } else {
                // Check email
                if ($this->current_model->findUserByEmail($data['email'])) {
                    $data['email_err'] = 'Email is already taken';
                }
            }

            // Validate Name
            if (empty($data['name'])) {
                $data['name_err'] = 'Pleae enter name';
            }

            // Validate Password
            if (empty($data['password'])) {
                $data['password_err'] = 'Pleae enter password';
            } elseif (strlen($data['password']) < 6) {
                $data['password_err'] = 'Password must be at least 6 characters';
            }

            // Validate Confirm Password
            if (empty($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Pleae confirm password';
            } else if ($data['password'] != $data['confirm_password']) {
                $data['confirm_password_err'] = 'Passwords do not match';
            }

            $valid_registration = (empty($data['email_err']) && empty($data['name_err']) && empty($data['password_err']) && empty($data['confirm_password_err']));
            if ($valid_registration) {
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                
                if ($this->current_model->register($data)) {
                    redirect('login');
                    flash('register_success', 'You are registered and can log in');
                } else {
                    die('Something went wrong');
                }
            } else {
                $this->form->set_field('name', 'register', 'value', $data['name']);
                $this->form->set_field('name', 'register', 'error', $data['name_err']);
                $this->form->set_field('email', 'register', 'value', $data['email']);
                $this->form->set_field('email', 'register', 'error', $data['email_err']);
                $this->form->set_field('password', 'register', 'error', $data['password_err']);
                $this->form->set_field('confirm_password', 'register', 'error', $data['confirm_password_err']);
                $this->vdata['form'] = $this->form->get_form_html();
                $this->view('users/register', $this->vdata);
            }
        } else {
            $this->vdata['form'] = $this->form->get_form_html();
            $this->view('users/register', $this->vdata);
        }
    }

    /**
     * Manage user registration.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function login()
    {
        $fieldsets = [
            'login' => [
                'id' => 'login-fields',
                'fields' => $this->loginFormFields
            ]
        ];
        $this->form->set_field_sets($fieldsets);
        $this->form->set_submit_text('login');
        $this->form->set_action(URLROOT . '/login');

        // Check for POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Process form
            // Sanitize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Init data
            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => '',
            ];

            if (empty($data['email'])) {
                $data['email_err'] = 'Pleae enter email';
            }

            if (empty($data['password'])) {
                $data['password_err'] = 'Please enter password';
            }

            $existing_user = $this->current_model->findUserByEmail($data['email']);
            if (! $existing_user) {
                $data['email_err'] = 'No user found';
            }

            $data_submitted = (empty($data['email_err']) && empty($data['password_err']));

            if ($data_submitted) {
                $loggedInUser = $this->current_model->login($data['email'], $data['password']);

                if ($loggedInUser) {
                    $this->createUserSession($loggedInUser);
                    $this->view('admin/index', $data);
                } else {
                    $data['password_err'] = 'Password incorrect';
                    $this->form->set_field('email', 'login', 'value', $data['email']);
                    $this->form->set_field('password', 'login', 'error', $data['password_err']);
                    $this->vdata['form'] = $this->form->get_form_html();
                    $this->view('users/login', $this->vdata);
                }
            } else {
                $this->form->set_field('email', 'login', 'error', $data['email_err']);
                $this->form->set_field('email', 'login', 'value', $data['email']);
                $this->form->set_field('password', 'login', 'error', $data['password_err']);
                $this->vdata['form'] = $this->form->get_form_html();
                $this->view('users/login', $this->vdata);
            }
        } else {
            $this->vdata['form'] = $this->form->get_form_html();
            $this->view('users/login', $this->vdata);
        }
    }

    private function error()
    {

    }

    /**
     * Log user out.
     * 
     * @since 1.0.0
     * 
     * @return void
     */
    public function logout()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();
        redirect('login');
    }

    /**
     * Create a new session for the user.
     * 
     * @since 1.0.0
     * 
     * @var object $user The user database object. 
     * @return void
     */
    private function createUserSession($user)
    {
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->name;
        redirect();
    }
}
