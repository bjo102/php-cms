<?php

/**
 * PHP CMS Controllers: Posts_Public
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Controllers
 */

/**
 * The public posts controller class.
 * 
 * Controller for viewing posts on the public webpages.

 * @since 2.0.0
 */
class Posts_Public extends Controller
{
    /**
     * The current URL in the browser.
     * 
     * @since 2.0.0
     * 
     * @var string
     */
    private $url;

    /**
     * The user model for this controller.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $user_model;

    /**
     * The post type model for this controller.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $pt_model;

    public function __construct()
    {
        $this->url = get_url(true);
        $this->current_model = $this->model('Post');
        $this->pt_model = $this->model('Post_Type');
        $this->user_model = $this->model('User');
    }

    /**
     * Load public archive or single post content, based on the current URL.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    public function index()
    {
        $view = '404';
        $pt = $this->pt_model->get_post_type($this->url[0], 'name');

        // post query
        if (isset($this->url[1])) {
            $post = $this->current_model->get_post($this->url[1], 'name', ['post_type' => $pt->id]);

            if ($post) {
                $view = 'posts/single';
                $data = [
                    'post_title' => $post->title,
                    'published' => $post->published,
                    'content' => $post->content,
                ];
            }

        // post type archive
        } else {
            $view = 'posts/index';
            $data = [
                'archive_title' => $pt->plural,
                'post_type_description' => $pt->description,
                'posts' => $this->current_model->get_posts(['post_type' => $pt->id])
            ];
        }

        $this->view($view, $data);
    }
}
