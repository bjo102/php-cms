<?php

/**
 * PHP CMS Controllers: Terms_Admin
 * 
 * @since 2.0.0
 * 
 * @package PHP_CMS\Controllers
 */

 /**
  * The taxonomy term controller class for the admin dashboard.
  * 
  * @since 2.0.0
  */
class Terms_Admin extends Admin_Controller
{
    /**
     * The taxonomies model for this controller.
     * 
     * @since 2.0.0
     * 
     * @var object
     */
    public $taxonomy_model;

    /**
     * The field set for the admin term editing form.
     * 
     * @since 2.0.0
     * 
     * @var array
     */
    public $term_edit_fieldset = array(
        'term_title' => [
            'type' => 'text',
            'label' => 'term title',
            'container_class' => 'field-width-50',
            'required' => true,
            'min' => 1,
            'max' => 100,
            'sql_val' => '',
        ],
        'term_name' => [
            'type' => 'text',
            'label' => 'term name',
            'container_class' => 'field-width-50',
            'min' => 1,
            'max' => 100,
        ],
    );

    public function __construct()
    {
        parent::__construct();
        $this->admin_base_url   = URLROOT . '/admin/?admin=terms';
        $this->current_model    = $this->model('Term');
        $this->taxonomy_model   = $this->model('Taxonomy');
    }

    /**
     * Load the admin index or edit form, based on the current URL.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    public function index()
    {
        if (!isset($_GET['term'])) {
            $this->get_index();
            exit();
        }

        $is_editor      = in_array($_GET['term'], ['edit', 'add']);
        $is_deletion    = ($_GET['term'] == 'delete' && isset($_GET['term_id']) && $this->current_model->get_term($_GET['term_id']));

        if ($is_editor) {
            $this->get_edit_form();
            exit();
        }

        if ($is_deletion) {
            $this->current_model->delete_term($_GET['term_id']);
        }
            
        $this->get_index();
    }

    /**
     * Load the primary index view of the admin dashboard.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    private function get_index()
    {
        $args = [];
        $is_taxonomy_set = (isset($_GET['taxonomy']) && $this->taxonomy_model->get_taxonomy($_GET['taxonomy'], 'name'));
        $this->vdata['title_prop'] = 'title';

        if ($is_taxonomy_set) {

            $tax = $this->taxonomy_model->get_taxonomy($_GET['taxonomy'], 'name');
            $args['taxonomy'] = intval($tax->id);
            $this->vdata['index_heading'] = $tax->plural;
            $this->vdata['add_new_link'] = $this->admin_base_url . '&term=add&taxonomy=' . $tax->id;
            $this->vdata['add_new_text'] = 'add new ' . $tax->plural . ' term';

        } else {

            $this->vdata['index_heading'] = 'All Terms';

        }

        $this->vdata['index_items'] = $this->current_model->get_terms($args);
        $this->vdata['index_item_edit_link'] = $this->admin_base_url . '&term=edit&term_id=';
        $this->vdata['index_item_delete_link'] = $this->admin_base_url . '&term=delete&term_id=';
        $this->view('admin/index', $this->vdata, 'admin');
    }

    /**
     * Load the view for editing terms with the controller form.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    private function get_edit_form()
    {
        $is_existing_term = ($_GET['term'] == 'edit' && $this->current_model->get_term($_GET['term_id']));
        $is_new_term = ($_GET['term'] == 'add' && $this->taxonomy_model->get_taxonomy($_GET['taxonomy']));
        $is_submission = $_SERVER['REQUEST_METHOD'] == 'POST';
        $form_args = [
            'form_id' => 'term-edit-form',
            'method' => 'post',
            'field_sets' => [
                'term-edit-fieldset' => [
                    'id' => 'term-edit-fieldset',
                    'fields' => $this->term_edit_fieldset
                ]
            ]
        ];
        $this->form = new Form($form_args);

        if ($is_existing_term) :

            $this->current_term = $this->current_model->get_term($_GET['term_id']);
            $this->current_method = 'update_term';
            $this->current_taxonomy = $this->taxonomy_model->get_taxonomy($this->current_term->taxonomy);
            $this->vdata['form-title'] = 'Edit term: <span id="">' . $this->current_term->title . '</span>';
            $this->vdata['delete-link'] = URLROOT . '/admin/&term=delete&term_id=' . $_GET['term_id'];
            $this->form->set_form_class('form-edit');
            $this->form->set_action($this->admin_base_url . '&term=edit&term_id=' . $this->current_term->id);
            $this->form->set_submit_text('update');
            $this->form->set_field('term_title', 'term-edit-fieldset', 'value', $this->current_term->title);
            $this->form->set_field('term_name', 'term-edit-fieldset', 'value', $this->current_term->name);

        elseif ($is_new_term) :

            $this->current_method = 'add_term';
            $this->current_taxonomy = $this->taxonomy_model->get_taxonomy($_GET['taxonomy']);
            $this->vdata['form-title'] = 'New ' . $this->current_taxonomy->title;
            $this->form->set_form_class('form-add-new');
            $this->form->set_action($this->admin_base_url . '&term=add&taxonomy=' . $this->current_taxonomy->id);
            $this->form->set_submit_text('add new');

        endif;

        if ($is_submission) {
            $this->process_post();
        }

        $this->vdata['form-type'] = $this->current_method;
        $this->vdata['form'] = $this->form->get_form_html();
        $this->view('admin/post', $this->vdata, 'admin');
    }

    /**
     * Handle submissions form the controller form.
     * 
     * Sanitize & validate post data for submission to the controller model.
     * 
     * @since 2.0.0
     * 
     * @return void
     */
    private function process_post()
    {
        $this->submission = [
            'title'         => trim($_POST['term_title']),
            'name'          => slugify_string($_POST['term_name']),
            'taxonomy'      => $this->current_taxonomy->id,
            'title_err'     => '',
            'name_err'      => '',
        ];

        if ($this->current_method == 'update_term') {
            $this->submission['id'] = $this->current_term->id;
        }

        if (empty($this->submission['title'])) {
            $this->submission['title_err'] = 'enter title';
        }

        if (empty($this->submission['name'])) {
            $this->submission['name'] = slugify_string($this->submission['title']);
        }

        $named_post = $this->current_model->get_term($this->submission['name'], 'name');
        $duplicate_names_check = (
            ($this->current_method == 'add_post' && $named_post) ||
            ($this->current_method == 'update_post' && $named_post && $named_post->id !== $this->submission['id'])
        );

        if ($duplicate_names_check) {
            $this->submission['name_err'] = 'There is already a post with the name "' . $this->submission['name'] . '"';
        }

        $valid_submission = (empty($this->submission['title_err']) && empty($this->submission['name_err']) && empty($this->submission['content_err']));
        
        if (!$valid_submission) {
            $this->form->set_field('term_title', 'term-edit-fieldset', 'value', $this->submission['title']);
            $this->form->set_field('term_title', 'term-edit-fieldset', 'error', $this->submission['title_err']);
            $this->form->set_field('term_name', 'term-edit-fieldset', 'value', $this->submission['name']);
            $this->form->set_field('term_name', 'term-edit-fieldset', 'error', $this->submission['name_err']);

            return;
        }

        $submission = call_user_func_array([$this->current_model, $this->current_method], [$this->submission]);

        if ($submission) {
            if ($this->current_method == 'add_term') {
                $this->current_term = $this->current_model->get_term(
                    $this->submission['name'],
                    'name',
                    ['taxonomy' => $this->current_taxonomy->id]
                );
            }
            flash('post_message', 'Term Added');
            redirect('admin?admin=terms&term=edit&term_id=' . $this->current_term->id);
        } else {
            die('Term was not added.');
        }
    }
}
