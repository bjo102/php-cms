<?php

/**
 * PHP CMS Bootstrap file.
 * 
 * @since 1.0.0
 * 
 * @package PHP_CMS\Library
 */

//error_reporting(E_ALL);

session_start();

// Load Config
require_once 'config.php';

// Load helpers
foreach (scandir(APPROOT . 'src/utils/') as $filename) {
    $path = APPROOT . 'src/utils/' . $filename;
    if (is_file($path)) {
        require $path;
    }
}

// Load core library
require_once 'lib/Form.php';
require_once 'lib/Model.php';
require_once 'lib/Controller.php';
require_once 'lib/Admin_Controller.php';
require_once 'lib/Database.php';

// Admin controllers
require_once 'controllers/Posts_Admin.php';
require_once 'controllers/Terms_Admin.php';

// Public controllers
require_once 'controllers/Pages.php';
require_once 'controllers/Posts_Public.php';
require_once 'controllers/Users_Public.php';

require_once 'lib/Init.php';